import './index.css'

import React, { Component } from 'react'

import App from './graphs/App.jsx'

export default class ExampleComponent extends Component {

  render() {

    return (
      <App />
    )
  }
}
