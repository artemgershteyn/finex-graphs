import 'moment/locale/ru'

import * as d3 from 'd3'

import React, { Component } from 'react'

import _ from 'lodash'
import data from './data'
import moment from 'moment'

export default class App extends Component {
  componentDidMount(){
    const velues = data.map(d=>type(d))
    var svg = d3.select("svg"),
      margin = {top: 20, right: 90, bottom: 110, left: 40},
      margin2 = {top: 430, right: 90, bottom: 30, left: 40},
      width = parseInt(d3.select("#chart").style("width")) - margin.left - margin.right,
      height = +svg.attr("height") - margin.top - margin.bottom,
      height2 = +svg.attr("height") - margin2.top - margin2.bottom

    var x = d3.scaleTime().range([0, width]),
      x2 = d3.scaleTime().range([0, width]),
      y = d3.scaleLinear().range([height, 0]),
      y2 = d3.scaleLinear().range([height2, 0]),
      y3 = d3.scaleLinear().range([height, 0]),
      y4 = d3.scaleLinear().range([height2, 0])

    var xAxis = d3.axisBottom(x).tickFormat(d3.timeFormat("%m.%y")).tickSizeInner(-height).tickSizeOuter(0).tickPadding(10),
      xAxis2 = d3.axisBottom(x2).tickFormat(d3.timeFormat("%m.%y")),
      yAxis = d3.axisRight(y).tickFormat(d => d.toLocaleString() + " ₽"),
      prcAxis = d3.axisLeft(y3).tickFormat(d3.format(".0%")),
      prcAxis2 = d3.axisLeft(y4).tickFormat(d3.format(".0%"))
    
    var brush = d3.brushX()
      .extent([[0, 0], [width, height2]])
      .on("brush end", brushed)

    var zoom = d3.zoom()
      .scaleExtent([1, Infinity])
      .translateExtent([[0, 0], [width, height]])
      .extent([[0, 0], [width, height]])
      .on("zoom", zoomed)
    
    var area = d3.area()
      // .curve(d3.curveStepBefore)
      .x(function(d) { return x(d.datestamp); })
      .y0(height)
      .y1(function(d) { return y(d.value); })

    var areaLine = d3.line()
    // .curve(d3.curveStepBefore)
      .x(function(d) { return x(d.datestamp); })
      .y(function(d) { return y(d.value); })
    
    var prc = d3.line()
      // .curve(d3.curveBundle)
      .x(function(d) { return x(d.datestamp); })
      .y(function(d) { return y3(d.rate); })


    var area2 = d3.area()
      .curve(d3.curveMonotoneX)
      .x(function(d) { return x2(d.datestamp); })
      .y0(height2)
      .y1(function(d) { return y2(d.value); })

    var prc2 = d3.line()
      .curve(d3.curveBundle)
      .x(function(d) { return x2(d.datestamp); })
      .y(function(d) { return y4(d.rate); })

    svg.append("defs").append("clipPath")
      .attr("id", "clip")
    .append("rect")
      .attr("width", width)
      .attr("height", height)

    var focus = svg.append("g")
      .attr("class", "focus")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")")

    var context = svg.append("g")
      .attr("class", "context")
      .attr("transform", "translate(" + margin2.left + "," + margin2.top + ")")

    x.domain(d3.extent(velues, function(d) { return d.datestamp; }));
    y.domain([0, d3.max(velues.map(val=>val.value))]);
    y3.domain([0, d3.max(velues.map(val=>val.rate))]);
    y4.domain([0, d3.max(velues.map(val=>val.rate))]);
    x2.domain(x.domain());
    y2.domain(y.domain());
    var myTool = d3.select(".graphBody")
    .append("div")
    .attr("class", "mytooltip")
    var withdrawTool = d3.select(".graphBody")
      .append("div")
      .attr("class", "withdrawtooltip")

    focus.append("g")
      .attr("class", "axis axis--x")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)
    focus.append("path")
      .datum(velues)
      .attr("class", "area")
      .attr("d", area)

    focus.append("path")
      .datum(velues)
      .attr("class", "line")
      .attr("d", areaLine)
      
    focus.append("path")
      .datum(velues)
      .attr("class", "prc")
      .attr("d", prc)


    focus.append("g")
        .attr("class", "axis axis--prc")
        .call(prcAxis);
  
    focus.append("text")
        .attr("x", 9)
        .attr("dy", ".35em");
    focus.append("g")
        .attr("class", "axis axis--y")
        .attr("transform", "translate(" + width + " ,0)")	
        .call(yAxis);

    context.append("path")
      .datum(velues)
      .attr("class", "area")
      .attr("d", area2);

    context.append("path")
      .datum(velues)
      .attr("class", "prc")
      .attr("d", prc2);
    var selectLineVertical = svg.append("g")
      .attr("class", "selectLineVertical")
    selectLineVertical.append('line')
      .attr('class', 'x-hover-line hover-line')
      .attr('y1' , 0)
      .attr('y2', height);
    var selectLineHorValue = svg.append("g")
      .attr("class", "selectLineHorValue")
    selectLineHorValue.append('line')
      .attr('class', 'x-hover-line hover-line')
      .attr('x1' , 0)
      .attr('x2', width);
    var selectLineHorRate = svg.append("g")
      .attr("class", "selectLineHorRate")
    selectLineHorRate.append('line')
      .attr('class', 'x-hover-line hover-line')
      .attr('x1' , 0)
      .attr('x2', width);
      
    context.append("g")
        .attr("class", "axis axis--x")
        .attr("transform", "translate(0," + height2 + ")")
        .call(xAxis2);

    context.append("g")
        .attr("class", "brush")
        .call(brush)
        .call(brush.move, x.range());

    svg.append("rect")
        .attr("class", "zoom")
        .attr("width", width)
        .attr("height", height)
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
        .call(zoom)
        .on("mousemove", mousemove);
      
    var selectValuePoint = svg.append("g")
      .attr("class", "selectValuePoint")
      // .style("display", "none");

    selectValuePoint.append("circle")
      .attr("r", 5)
      .attr("class","value-circle")

    var selectPoint = svg.append("g")
        .attr("class", "selectPoint")
        // .style("display", "none");
  
      selectPoint.append("circle")
        .attr("r", 5)
        .attr('fill',"#25CE77")
    var bisectDate = d3.bisector(function(d) { return d.datestamp; }).left
    var tickPos = x.range();
    function mousemove() {
      const x0 = x.invert(d3.mouse(d3.event.currentTarget)[0]),
        i = bisectDate(velues, x0, 1),
        d0 = velues[i - 1],
        d1 = velues[i];
      
    const rangeValueOfFirst = x(d0.datestamp),
        rangeValueOfSecond = x(d1.datestamp),
        rangeValueOfMousePos = d3.mouse(d3.event.currentTarget)[0],
        closestD = Math.abs(rangeValueOfMousePos - rangeValueOfFirst) > Math.abs(rangeValueOfMousePos - rangeValueOfSecond) ? d1 : d0;
    let withdrawToolColor = '#25CE77',
      withdrawToolText = '',
      withdrawX = 0,
      withdrawY = 0
    closestD.balance<0?withdrawToolText='Вывод':withdrawToolText='Ввод'
    closestD.balance<0?withdrawToolColor='#FF9933':withdrawToolColor='#25CE77'
    if(closestD.balance!=0){
      withdrawTool.html(
        `
          <div>
            <span>${withdrawToolText} средств: </span>
            <span style='color: ${withdrawToolColor};'>${Math.abs(closestD.balance).toLocaleString()} ₽</span>
          </div>
        `
      ) 
      .style("left", () => (margin.left + x(closestD.datestamp) + 20) + "px")   
      .style("top", () => (margin.top + y(closestD.value)) + "px")
      .style("opacity", 1)
    }else{
      withdrawTool.style("opacity", 0)
    }
    
    var ruDate = moment(closestD.datestamp)
    ruDate.locale('ru')
    var tooltipX = 0
    if(d3.mouse(d3.event.currentTarget)[0]<width/2) tooltipX = d3.event.pageX + 10
    else tooltipX = d3.event.pageX - 300
    var tooltipY = 0
    if(d3.mouse(d3.event.currentTarget)[1]<height/2) tooltipY = d3.event.pageY + 10
    else tooltipY = d3.event.pageY - 160
    myTool.html(
      `<div id='income-tooltip'>
        <div class='date'>${ruDate.format('DD MMMM YYYY')}</div>
        <div>
        <div class='text-row'>
          <span>Доходность</span>
          <span class='val prc'>${(closestD.rate*100).toFixed(1).toLocaleString()} %</span>
        </div>
        <div class='text-row'>
          <span>Cтоимость активов с учетом ввода / вывода</span>
          <span class='val'>${Math.floor(closestD.value).toLocaleString()} ₽</span>
        </div>
      </div>`)
      .style("left", tooltipX + "px")   
      .style("top", tooltipY + "px")
    const focus = d3.select(".selectPoint");
    focus.attr("transform", () => "translate(" + (margin.left + x(closestD.datestamp)) + "," + (margin.top + y3(closestD.rate)) + ")");
    const selectValue = d3.select(".selectValuePoint");
    const selectLineVertical = d3.select('.selectLineVertical')
    const slctLnHorVal = d3.select('.selectLineHorValue')
    const slctLnHorRate = d3.select('.selectLineHorRate')

    slctLnHorRate.attr("transform", () => "translate(" + margin.left + ","+ (margin.top + y3(closestD.rate)) +")");
    slctLnHorVal.attr("transform", () => "translate(" + margin.left + ","+ (margin.top + y(closestD.value)) +")");
    selectLineVertical.attr("transform", () => "translate(" + (margin.left + x(closestD.datestamp)) + ","+margin.top +")");
    selectValue.attr("transform", () => "translate(" + (margin.left + x(closestD.datestamp)) + "," + (margin.top + y(closestD.value)) + ")");
    if(closestD.balance<0) d3.select('.value-circle').attr('fill','#FF9933')
    if(closestD.balance>0) d3.select('.value-circle').attr('fill','#25CE77')
    if(closestD.balance==0) d3.select('.value-circle').attr('fill','#000')
    
    
    
    }

    function brushed() {
      if (d3.event.sourceEvent && d3.event.sourceEvent.type === "zoom") return; // ignore brush-by-zoom
      var s = d3.event.selection || x2.range();
      x.domain(s.map(x2.invert, x2));
      var domain= x.domain();
      y.domain([d3.min(data.map(function(d) { 	  
        if(parseInt(d.datestamp.getTime())> parseInt(domain[0].getTime()) && parseInt(d.datestamp.getTime()) < parseInt(domain[1].getTime())){
        return parseFloat(d.value);  
        }})), d3.max(data.map(function(d) { 
        
        if(parseInt(d.datestamp.getTime())> parseInt(domain[0].getTime()) && parseInt(d.datestamp.getTime()) < parseInt(domain[1].getTime())){
        return d.value;
      }}))]);   
      y3.domain([d3.min(data.map(function(d) { 	  
        if(parseInt(d.datestamp.getTime())> parseInt(domain[0].getTime()) && parseInt(d.datestamp.getTime()) < parseInt(domain[1].getTime())){
        return d.rate;  
        }})), d3.max(data.map(function(d) { 
        
        if(parseInt(d.datestamp.getTime())> parseInt(domain[0].getTime()) && parseInt(d.datestamp.getTime()) < parseInt(domain[1].getTime())){
        return d.rate;
      }}))]);
      focus.select(".area").attr("d", area);
      focus.select(".line").attr("d", areaLine);
      focus.select(".prc").attr("d", prc);
      focus.select(".axis--x").call(xAxis);
      focus.selectAll(".data-circle").remove()
      focus.select(".axis--y").call(yAxis);
      focus.select(".axis--prc").call(prcAxis);
      focus.selectAll("line-circle")
        .data(velues.filter(function(d) { 
          if(d.balance>0) return d;
         }))
      .enter().append("circle")
        .attr("class", "data-circle")
        .attr("r", 3)
        .attr('fill','#25CE77')
        .attr("cx", function(d) { return x(d.datestamp); })
        .attr("cy", function(d) { return y(d.value); });
      focus.selectAll("line-circle")
          .data(velues.filter(function(d) { 
            if(d.balance<0) return d;
           }))
        .enter().append("circle")
          .attr("class", "data-circle")
          .attr("r", 3)
          .attr('fill','#FF9933')
          .attr("cx", function(d) { return x(d.datestamp); })
          .attr("cy", function(d) { return y(d.value); });
      svg.select(".zoom").call(zoom.transform, d3.zoomIdentity
          .scale(width / (s[1] - s[0]))
          .translate(-s[0], 0));
    }
    
    function zoomed() {
      if (d3.event.sourceEvent && d3.event.sourceEvent.type === "brush") return; // ignore zoom-by-brush
      var t = d3.event.transform;
      x.domain(t.rescaleX(x2).domain());
      var domain= x.domain();
      y.domain([d3.min(data.map(function(d) { 	  
        if(parseInt(d.datestamp.getTime())> parseInt(domain[0].getTime()) && parseInt(d.datestamp.getTime()) < parseInt(domain[1].getTime())){
        return parseFloat(d.value);  
        }})), d3.max(data.map(function(d) { 
        
        if(parseInt(d.datestamp.getTime())> parseInt(domain[0].getTime()) && parseInt(d.datestamp.getTime()) < parseInt(domain[1].getTime())){
        return d.value;
      }}))]);   
      y3.domain([d3.min(data.map(function(d) { 	  
        if(parseInt(d.datestamp.getTime())> parseInt(domain[0].getTime()) && parseInt(d.datestamp.getTime()) < parseInt(domain[1].getTime())){
        return d.rate;  
        }})), d3.max(data.map(function(d) { 
        
        if(parseInt(d.datestamp.getTime())> parseInt(domain[0].getTime()) && parseInt(d.datestamp.getTime()) < parseInt(domain[1].getTime())){
        return d.rate;
      }}))]);
      focus.select(".area").attr("d", area);
      focus.select(".line").attr("d", areaLine);
      focus.selectAll(".data-circle").remove()
      focus.select(".prc").attr("d", prc);
      focus.select(".axis--x").call(xAxis);
      focus.select(".axis--y").call(yAxis);
      focus.select(".axis--prc").call(prcAxis);
      context.select(".brush").call(brush.move, x.range().map(t.invertX, t));
      focus.selectAll("line-circle")
        .data(velues.filter(function(d) { 
          if(d.balance>0) return d;
         }))
      .enter().append("circle")
        .attr("class", "data-circle")
        .attr("r", 3)
        .attr('fill','#25CE77')
        .attr("cx", function(d) { return x(d.datestamp); })
        .attr("cy", function(d) { return y(d.value); });
      focus.selectAll("line-circle")
        .data(velues.filter(function(d) { 
          if(d.balance<0) return d;
         }))
      .enter().append("circle")
        .attr("class", "data-circle")
        .attr("r", 3)
        .attr('fill','#FF9933')
        .attr("cx", function(d) { return x(d.datestamp); })
        .attr("cy", function(d) { return y(d.value); });
    }
    
    function type(d) {
      d.datestamp = moment(d.datestamp).toDate()
      d.value = +parseFloat(d.value);
      return {
        datestamp: d.datestamp,
        value: d.value,
        rate: d.rate,
        balance: d.balance
      }
    }

    
  }
  render () {
    

    return (
      <div className='graphBody' >
        <svg id='chart' height='500'>
        <linearGradient id="linear-gradient" y1="0%" x2="0%" y2="100%">
          <stop offset="0%" stopColor="rgba(76,84,81,15)" stopOpacity=".2"/>
          <stop offset="100%" stopColor="transparent" stopOpacity="0"/>
        </linearGradient>
        </svg>
      </div>
    )
  }
}
